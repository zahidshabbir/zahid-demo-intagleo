//
//  Constants.swift
//  ZahidDemo
//
//  Created by Zahid Shabbir on 27/02/2021.
    import Foundation
struct Constants {
    struct Apis {
        static let albumUrl = "https://jsonplaceholder.typicode.com/photos"
    }
    struct Cells {
        static let albumCellIdentifier = "AlbumTableViewCell"
    }
}
